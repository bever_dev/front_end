pipeline {
    agent any

    tools {
        dockerTool 'bever-docker'
    }

    stages {

        stage('SonarQube Analysis') {
            //when { equals expected: true, actual: false } // temp disable test
            environment {
                sonar = tool 'sonarqube-4.7.0'
            }
            
            steps {
                updateGitlabCommitStatus name: 'pipeline', state: 'running'
                
                withSonarQubeEnv(installationName: 'bever-sonarcloud', credentialsId: 'sonarcloud-token') {
                    sh ''' ${sonar}/bin/sonar-scanner \
                    -Dsonar.projectKey=bever-front '''
                }
            }
        }

        stage('Build image') {
            steps {
                sh "docker build . -t ultimatehikari/bever-front:1.${env.BUILD_NUMBER}"
            }
        }

        stage('Push image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'dockerhub-password', passwordVariable: 'password', usernameVariable: 'user')]){
                    sh "docker login -u ${env.user} -p ${env.password}"
                    sh "docker push ultimatehikari/bever-front:1.${env.BUILD_NUMBER}"
                }
            }
        }

        stage('Kubenetes deploy') {
            steps {
                withCredentials([file(credentialsId: 'kubeconfig', variable: 'CONFIG')]) {
                    sh "kubectl set image deployment/front front-pod=ultimatehikari/bever-front:1.${env.BUILD_NUMBER} --kubeconfig=\"$CONFIG\""
                }
            }
        }
    }

    post {
        failure {
            updateGitlabCommitStatus name: 'pipeline', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'pipeline', state: 'success'
        }
        aborted {
            updateGitlabCommitStatus name: 'pipeline', state: 'canceled'
        }
    }
}