# ========= BUILD =========
FROM node:16-alpine as builder

WORKDIR /app

COPY package.json .
COPY package-lock.json .
RUN npm install --legacy-deps

COPY . .

RUN npm run build

# ========= RUN =========
FROM nginx:1.17

COPY --from=builder /app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/

ENTRYPOINT ["nginx", "-g", "daemon off;"]
