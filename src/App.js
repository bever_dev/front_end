import {Route, Routes} from 'react-router-dom';
import 'mdb-ui-kit/css/mdb.min.css';
import Home from './general/components/Home/Home';
import SignUp from './auth/components/SignUp';
import Profile from './profile/components/Profile';
import Layout from './general/components/Layout';
import SignIn from "./auth/components/SignIn";
import BeveragePage from "./library/components/DetailBeveragePage/BeveragePage";
import BeverageGallery from "./library/components/BeverageGallery";
import AdminPanel from "./profile/admin/AdminPanel";
import AuthService from "./auth/AuthService";
import PrivateRoute from "./general/components/PrivateRoute";
import {Navigate} from "react-router-dom";
import BeverageReviewPage from "./library/components/BeverageReviewPage/BeverageReviewPage";
import Page404 from "./general/components/Page404";
import UsersList from "./profile/admin/UsersList";
import UserFavorites from "./prefer/component/UserFavorites";
import UserService from "./profile/UserService";

export default function App() {

    return (
        <div>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<Home/>}/>
                    <Route path="signup" element={<SignUp/>}/>
                    <Route path="signin" element={<SignIn/>}/>
                    <Route path="profile">
                        <Route index element={<PrivateRoute isAccessAllowed={UserService.isAuth()} element={<Profile/>}
                                                            redirectPath="/signin"/>}/>
                        <Route path="favorites" element={<PrivateRoute isAccessAllowed={UserService.isAuth()} element={<UserFavorites/>}
                                                                       redirectPath="/signin"/>}/>
                    </Route>
                    <Route path="admin">
                        <Route index element={<PrivateRoute isAccessAllowed={UserService.isModerator()} element={<AdminPanel/>} redirectPath="/"/>}/>
                        <Route path="reviews" element={<PrivateRoute isAccessAllowed={UserService.isModerator()} element={<BeverageReviewPage/>} redirectPath="/"/>}/>
                        <Route path='users' element={<PrivateRoute isAccessAllowed={UserService.isAdmin()} element={<UsersList/>} redirectPath="/"/>}/>
                    </Route>
                    <Route path="library">
                        <Route index element={<BeverageGallery/>}/>
                        <Route path="beverage/:beverageId" element={<BeveragePage/>}/>
                        <Route path="*" element={<Navigate to="" replace/>}/>
                    </Route>
                    <Route path="*" element={<Page404/>}/>
                </Route>
            </Routes>
        </div>
    );
}
