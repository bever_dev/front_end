import axios from "axios";
import { AuthUrl  } from "../url";

const register = (nickname, firstName, surname, middleName, password, email) => {
    return axios.post(AuthUrl + "signup", {
        nickname,
        firstName,
        surname,
        middleName,
        password,
        email
    });
};

const login = (nickname, password) => {
    return axios
        .post(AuthUrl + "signin", {
            nickname,
            password,
        })
        .then((response) => {
            if (response.data.jwtToken) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem("user");
    return null;
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

const AuthService = {
    register,
    login,
    logout,
    getCurrentUser,
};

export default AuthService;
