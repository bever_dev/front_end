import React, {useState} from "react";
import {Container, Button, Row, Col, Form, FormControl, Alert, Card, CardImg} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import AuthService from "../AuthService";

export default function SignIn() {

    let navigate = useNavigate();

    const [message, setMessage] = useState("");

    const [form, setForm] = useState({
        nickname: '',
        password: '',
    })

    const handleChange = (e) => {
        setForm({...form, [e.target.name]: e.target.value})
    }

    const handleSubmit = (e) => {

        e.preventDefault();

        AuthService.login(form.nickname, form.password)
            .then(() => {
                    navigate("/profile");
                    window.location.reload();
                },
                (error) => {
                    const msg =
                        (error.response && error.response.data && error.response.data.message) ||
                        error.message || error.toString();
                    setMessage(msg);
                }
            )

    }

    return (
        <Container className="md mt-3 d-flex justify-content-center">
            <Card className="card-container">
                <CardImg
                    src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
                    alt="profile-img"
                    className="profile-img-card"
                />
                <Form>
                    <Form.Group className="mb-3" controlId="nicknameId">
                        <Form.Label>Nickname</Form.Label>
                        <Form.Control
                            type="text"
                            name="nickname"
                            placeholder="Nickname..."
                            onChange={handleChange}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="passwordId">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            name="password"
                            placeholder="Password..."
                            onChange={handleChange}
                            required
                        />
                    </Form.Group>
                    <Button type="submit" className="me-4 btn btn-success btn-lg btn-block"
                            onClick={handleSubmit}>
                        Sign In
                    </Button>
                    {message && (
                        <Form.Group className="mt-3">
                            <Alert className="alert-danger" role="alert">
                                {message}
                            </Alert>
                        </Form.Group>
                    )}
                </Form>
            </Card>
        </Container>
    )

}
