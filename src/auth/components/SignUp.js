import React, {useState} from "react";
import {Alert, Button, Container, Form, Row} from "react-bootstrap";
import AuthService from "../AuthService";
import {useNavigate} from "react-router-dom";

export default function SignUp() {
    const navigate = useNavigate();

    const [message, setMessage] = useState("");

    const [form, setForm] = useState({
        nickname: '',
        firstName: '',
        middleName: '',
        surname: '',
        password: '',
        confPassword: '',
        email: ''
    })

    const handleChange = (e) => {
        setForm({...form, [e.target.name]: e.target.value })
    }

    const handleReset = () => {
        setForm({
            nickname: '',
            firstName: '',
            middleName: '',
            surname: '',
            password: '',
            confPassword: '',
            email: ''
        });
    }

    const handleSubmit = (e) => {
        const config = {
            headers: {
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Methods': 'POST, GET',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                'Content-Type': 'application/json',
            }
        }

        e.preventDefault();

        AuthService.register(
            form.nickname,
            form.firstName,
            form.surname,
            form.middleName,
            form.password,
            form.email
        ).then(
            (response) => {
                setMessage(response.data.message);
                navigate('/signin')
            },
            (error) => {
                const msg =
                    (error.response && error.response.data && error.response.data.message) ||
                    error.message || error.toString();
                setMessage(msg);
            }
        );
     }

    return (
        <Container className="md mt-3 d-flex justify-content-center">
            <Form className="col col-6">
                <Row className="mb-3 justify-content-center">
                    <Form.Group controlId="firstnameId" className="col col-sm-4">
                        <Form.Label>Firstname</Form.Label>
                        <Form.Control
                            type = "text"
                            name = "firstName"
                            placeholder="Firstname..."
                            onChange={handleChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="surnameId" className="col col-sm-4">
                        <Form.Label>Surname</Form.Label>
                        <Form.Control
                            type = "text"
                            name = "surname"
                            placeholder="Surname..."
                            onChange={handleChange}
                        />
                    </Form.Group>
                    <Form.Group controlId="middlenameId" className="col col-sm-4">
                        <Form.Label>Middlename</Form.Label>
                        <Form.Control
                            type = "text"
                            name = "middleName"
                            placeholder="Middlename..."
                            onChange={handleChange}
                        />
                    </Form.Group>
                </Row>
                <Row className="justify-content-center">
                    <Form.Group className="mb-3 col col-sm-6" controlId="nicknameId" >
                        <Form.Label>Nickname</Form.Label>
                        <Form.Control
                            type = "text"
                            name = "nickname"
                            placeholder="Nickname..."
                            onChange={handleChange}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3 col col-sm-6" controlId="emailId">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type = "text"
                            name = "email"
                            placeholder="Email..."
                            onChange={handleChange}
                        />
                    </Form.Group>
                </Row>
                <Form.Group className="mb-3 col col-sm-4" controlId="passwordId">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type = "password"
                        name = "password"
                        placeholder="Password..."
                        onChange={handleChange}
                    />
                </Form.Group>
                <Form.Group className="mb-3 col col-sm-4" controlId="confPasswordId">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type = "password"
                        name = "confPassword"
                        placeholder="Confirm Password..."
                        onChange={handleChange}
                    />
                </Form.Group>
                <div className="btn-toolbar">
                    <Button type="submit" className="btn btn-lg btn-block btn-success"
                            onClick={handleSubmit}>
                        Sign up
                    </Button>
                    <Button type="submit" className="btn btn-lg btn-block btn-danger"
                            onClick={handleReset}>
                        Reset
                    </Button>
                </div>
                {message && (
                    <Form.Group className="mt-3">
                        <Alert className="alert-danger" role="alert">
                            {message}
                        </Alert>
                    </Form.Group>
                )}
            </Form>
        </Container>
    )

}
