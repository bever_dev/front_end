import axios from "axios";
import {PictureURL} from "../url";

const uploadBeveragePictureAndGetUUID = (imgFile) => {
    console.log(imgFile)
    const fd = new FormData()
    fd.append('file', imgFile, imgFile.name)
    return axios.post(PictureURL + "upload", fd)
}

const PictureService = {
    uploadBeveragePictureAndGetUUID,
}

export default PictureService