import axios from "axios";
import AuthHeader from "../general/util/AuthHeader";
import { LibraryUrl } from "../url";

const getBeveragesPageFiltered = (pageState, filter) => {
    return axios.get(LibraryUrl + "filter", {
            headers: AuthHeader(),
            params: {
                number: pageState.currentPage - 1,
                size: pageState.pageSize,
                ...(filter.name !== "" ? { name: filter.name } : {}),
                ...(filter.creator !== "" ? { creator: filter.creator } : {}),
                ...(filter.category !== "" ? { category: filter.category } : {}),
                ...(filter.volume !== "" ? { volume: filter.volume } : {}),
                ...(filter.percentage !== "" ? { percentage: filter.percentage } : {}),
                ...(filter.composition !== "" ? { composition: filter.composition } : {})
            }
        }
    );
}

const getBeverageDetails = (beverageId) => {
    return axios.get(LibraryUrl + "beverage", {
            headers: AuthHeader(),
            params: {
                id: beverageId
            }
        }
    );
}

const addBeverage = (beverage) => {
    return axios.get(LibraryUrl + "add", {
            headers: AuthHeader(),
            params: {
                name: beverage.name,
                creator: beverage.creator,
                category: beverage.category,
                volume: beverage.volume,
                percentage: beverage.percentage,
                price: beverage.price,
                composition: beverage.composition,
                rating: beverage.rating,
                available: beverage.available,
                image: beverage.image
            }
        }
    );
}

const LibraryService = {
    getBeveragesPageFiltered,
    getBeverageDetails,
    addBeverage,
};

export default LibraryService;

