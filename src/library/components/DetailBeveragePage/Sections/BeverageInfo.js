import React, {useEffect, useState} from 'react'
import '../beverage_style_page.css'
import "bootstrap-icons/font/bootstrap-icons.css";
import PreferService from "../../../../prefer/PreferService";

function BeverageInfo({Beverage, userInfo}) {
    const [checked, setChecked] = useState({})

    useEffect(() => {
        if (Beverage.id !== undefined && userInfo.id !== undefined) {
            PreferService.checkIfBeverageInUserFavorites(Beverage.id, userInfo.id)
                .then(res => {
                        setChecked(res.data)
                    })
        }
    }, [Beverage, userInfo])

    const toggle = () => {
        if (checked === false) {
            PreferService.addBeverageInUserFavorite(Beverage.id, userInfo.id)
                .then(res => {
                    console.log(res)
                })
        } else {
            PreferService.deleteFavoriteByItsIdAndUserId(Beverage.id, userInfo.id)
                .then(res => {
                    console.log(res)
                })
        }
        setChecked(!checked)
    }

    return (
        <div className="container">
            <div className="beverageInfo">
                <div className="row g-0">
                    <div className="col-md-12">
                        <div className="p-2 right-side">
                            <div className="d-flex justify-content-between align-items-center">
                                <h3>{Beverage.name}</h3>
                                <div>
                                    <button type="button" className="btn btn-danger" data-bs-toggle="button"
                                            onClick={()=>toggle()}>
                                        {checked ?
                                            <i className="bi bi-heart-fill"/> :
                                            <i className="bi bi-heart"/>
                                        }
                                    </button>
                                </div>
                            </div>
                            <div className="row">
                                <dt className="col-sm-3">Категория</dt>
                                <dd className="col-sm-9">
                                    <p>{Beverage.category}</p>
                                </dd>
                                <dt className="col-sm-3">Объём (л)</dt>
                                <dd className="col-sm-9">
                                    <p>{Beverage.volume}</p>
                                </dd>
                                <dt className="col-sm-3">Крепость</dt>
                                <dd className="col-sm-9">
                                    <p>{Beverage.percentage}</p>
                                </dd>
                                <dt className="col-sm-3">Изготовитель</dt>
                                <dd className="col-sm-9">
                                    <p>{Beverage.creator}</p>
                                </dd>
                                <dt className="col-sm-3">Состав</dt>
                                <dd className="col-sm-9">
                                    <p>{Beverage.composition}</p>
                                </dd>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default BeverageInfo;