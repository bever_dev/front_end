import React from 'react'
import BeverageComment from "./BeverageComment";

function BeverageCommentsBlock({feedbacks, setFeedbacks}) {
    return (
        <div className="container my-5 py-5 text-dark">
            <div className="row d-flex justify-content-center">
                <div className="col-md-11 col-lg-10 col-xl-7">
                    {feedbacks.map((feedback, i) => (
                        <BeverageComment key={i} {...feedback} setFeedbacks={setFeedbacks}/>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default BeverageCommentsBlock;