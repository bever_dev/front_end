import ReactStars from "react-rating-stars-component";
import {useContext} from "react";
import {ButtonContext} from "../../BeverageReviewPage/BeverageReviewPage";
import {PictureURL} from "../../../../url";

export default function BeverageComment({
                                            userName,
                                            profileImgUUID,
                                            feedbackId,
                                            feedbackBody,
                                            feedbackRating,
                                        }) {
    const {approveFeedback, removeFeedback} = useContext(ButtonContext)
    return (<div className="d-flex flex-start mb-4">
            <img className="rounded-circle shadow-1-strong me-3"
                 src={PictureURL + "show?uuid=" + profileImgUUID} alt="avatar"
                 style={{height: "65px", width:"10%"}}/>
            <div className="card w-100" style={{borderColor: "#000000", borderStyle: "solid"}}>
                <div className="card-body p-4">
                    <div className="">
                        <h5>{userName}</h5>
                        <p>{feedbackBody}</p>
                        Оценка товара: <div className="col-sm-9 ">
                        <ReactStars
                            count={5}
                            size={22}
                            edit={false}
                            value={feedbackRating}
                        />
                    </div>
                    </div>
                    {approveFeedback ?
                        <div className="d-flex justify-content-between">
                            <div className="d-flex justify-content-between mt-3">
                                <i className="fas fa-long-arrow-alt-right ms-10"/>
                                <button type="button" className="btn btn-info"
                                        onClick={() => approveFeedback(feedbackId)}>
                                    Approve feedback
                                </button>
                            </div>
                            <div className="d-flex justify-content-between mt-3">
                                <i className="fas fa-long-arrow-alt-right ms-10"/>
                                <button type="button" className="btn btn-danger"
                                        onClick={() => removeFeedback(feedbackId)}>
                                    Remove feedback
                                </button>
                            </div>
                        </div> : <></>}
                </div>
            </div>
        </div>
    );
}
