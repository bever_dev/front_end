import React, {useEffect, useState} from 'react'
import ReactStars from "react-rating-stars-component"
import ReviewService from "../../../ReviewService";
import {PictureURL} from "../../../../url";

export default function PostBlock({nickname, beverageId, profileImgUUID}) {
    const [feedback, setFeedback] = useState({
        userName: "",
        beverageId: "",
        feedbackBody: "",
        feedbackRating: 0,
        profileImgUUID: "",
    })

    useEffect(() => {
        setFeedback({
            userName: nickname,
            beverageId: beverageId,
            feedbackBody: "",
            feedbackRating: 0,
            profileImgUUID: profileImgUUID
        })
    }, [nickname, beverageId, profileImgUUID])

    const submit = e => {
        e.preventDefault()
        console.log(feedback)
        ReviewService.postFeedback(feedback).then(
            response => {
                console.log(response)
            }
        )
        setFeedback({...feedback, feedbackRating: 0, feedbackBody: ""})
    }

    return (
        <div className="container d-flex justify-content-center pb-5 text-dark">
            <div className="col-xl-9">
                <div className="card w-100" style={{borderColor: "#000000", borderStyle: "solid"}}>
                    <div className="card-body p-4">
                        <div className="d-flex flex-start">
                            <img className="rounded-circle shadow-1-strong me-3"
                                 src={PictureURL + "show?uuid=" + profileImgUUID} alt="avatar"
                                 style={{height: "65px", width:"10%"}}/>
                            <div className="w-100">
                                <div className="p-2">
                                    Ваша оценка товара
                                    <ReactStars
                                        count={5}
                                        size={22}
                                        value={feedback.feedbackRating}
                                        onChange={newRating => {
                                            setFeedback({...feedback, feedbackRating: newRating})
                                        }}
                                    />
                                </div>
                                <div className="form-outline">
                                    <label>Ваш отзыв</label>
                                    <textarea className="form-control" id="textAreaExample" rows="5"
                                              style={{borderStyle: "inset", borderWidth: "2px"}}
                                              value={feedback.feedbackBody}
                                              onChange={e => setFeedback({...feedback, feedbackBody: e.target.value})}/>
                                </div>
                                <div className="d-flex justify-content-between mt-3">
                                    <i className="fas fa-long-arrow-alt-right ms-10"/>
                                    <button type="button" className="btn btn-danger" onClick={(e) => submit(e)}>
                                        Send for review
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
