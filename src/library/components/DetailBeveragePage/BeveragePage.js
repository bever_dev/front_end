import {useParams} from "react-router-dom";
import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from 'react-bootstrap';
import LibraryService from "../../LibraryService";
import BeverageInfo from "./Sections/BeverageInfo";
import BeverageCommentsBlock from "./Sections/BeverageCommentsBlock";
import PostBlock from "./Sections/PostBlock";
import ReviewService from "../../ReviewService";
import UserService from "../../../profile/UserService";
import {PictureURL} from "../../../url";
import {MDBCardImage} from "mdb-react-ui-kit";


function BeveragePage() {
    const {beverageId} = useParams();
    const [Beverage, setBeverage] = useState([])
    const [userInfo, setUserInfo] = useState([])
    const [feedbacks, setFeedbacks] = useState([])

    useEffect(() => {
        LibraryService.getBeverageDetails(beverageId)
            .then(response => {
                console.log(response)
                setBeverage(response.data.beverage[0])
            })
        UserService.getDefaultUserInfo().then(response => {
            setUserInfo(response.data)
        })
        ReviewService.getFeedbackForBeverage(beverageId)
            .then(response => {
                setFeedbacks(response.data)
            })
    }, [])

    return (
        <Container>
            <div className="postPage">
                <br/>
                <br/>
                <Row>
                    <Col>
                        <img className="w-100 h-100" src={PictureURL + "show?uuid=" + Beverage.image} alt="Relative image"/>
                    </Col>
                    <Col>
                        <BeverageInfo Beverage={Beverage} userInfo={userInfo}/>
                    </Col>
                </Row>
                <hr className="bg-danger border-2 border-top border-danger"/>
                <section className="rounded" style={{backgroundColor: "#e5e5e5", borderStyle: "ridge"}}>
                    <BeverageCommentsBlock feedbacks={feedbacks}/>
                    <PostBlock {...{
                        ...userInfo,
                        beverageId
                    }}/>
                </section>
            </div>
        </Container>
    )
}

export default BeveragePage;