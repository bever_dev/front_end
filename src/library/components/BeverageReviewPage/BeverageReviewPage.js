import {createContext, useEffect, useRef, useState} from 'react'
import BeverageCommentsBlock from "../DetailBeveragePage/Sections/BeverageCommentsBlock";
import ReviewService from "../../ReviewService";

export const ButtonContext = createContext({});

function BeverageReviewPage() {

    const [feedbacks, setFeedbacks] = useState([])

    useEffect(() => {
        ReviewService.getFeedbackForReview()
            .then(response => {
                console.log(response)
                setFeedbacks(response.data)
            })
    }, [])

    const removeFeedback = feedbackId => {
        const newFeedbacks = feedbacks.filter(feedback => feedback.feedbackId !== feedbackId)
        setFeedbacks(newFeedbacks)
        ReviewService.removeFeedback(feedbackId).then(
            response => {
                console.log(response)
            })
    }

    const approveFeedback = feedbackId => {
        const newFeedbacks = feedbacks.filter(feedback => feedback.feedbackId !== feedbackId)
        setFeedbacks(newFeedbacks)
        ReviewService.approveFeedback(feedbackId).then(
            response => {
                console.log(response)
            })
    }

    return (feedbacks.length > 0 ?
        <ButtonContext.Provider value={{feedbacks, setFeedbacks, approveFeedback, removeFeedback}}>
            <BeverageCommentsBlock feedbacks={feedbacks}/>
        </ButtonContext.Provider>
        :
        <div className='d-flex justify-content-center align-items-center'>
            <text className='mt-3 display-6'>No feedbacks for review yet. </text>
        </div>)
}

export default BeverageReviewPage;
