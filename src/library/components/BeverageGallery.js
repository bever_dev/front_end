import React, {useEffect, useLayoutEffect, useState} from "react";
import Pagination from "@vlsergey/react-bootstrap-pagination";
import {Grid} from "@mui/material";
import "bootstrap-icons/font/bootstrap-icons.css"
import {useNavigate} from "react-router-dom";

import {
    MDBBtn,
    MDBBtnGroup,
    MDBCard,
    MDBCardBody,
    MDBCardFooter,
    MDBCardHeader,
    MDBCardImage,
    MDBCardText,
    MDBCardTitle,
    MDBCol,
    MDBContainer,
    MDBInputGroup,
    MDBRow
} from "mdb-react-ui-kit";
import LibraryService from "../LibraryService";
import {PictureURL} from "../../url";
import PreferService from "../../prefer/PreferService";

export default function BeverageGallery() {

    const [beverages, setBeverages] = useState([]);
    const [likes, setLikes] = useState(new Map())
    const [errorMsg, setErrorMsg] = useState("");

    let navigate = useNavigate();

    const [pagesState, setPagesState] = useState({
        currentPage: 1,
        totalPages: 1,
        pageSize: 8
    });

    const [filter, setFilter] = useState({
        name: "",
        creator: "",
        category: "",
        volume: "",
        percentage: "",
        composition: ""
    })

    function getBeveragesInfoData() {
        LibraryService.getBeveragesPageFiltered(pagesState, filter).then(
            (response) => {
                setBeverages(response.data.beverage)
                response.data.beverage.forEach(beverage => {
                    PreferService.getAllFavoritesByBeverage(beverage.id)
                        .then(res => {
                            setLikes(new Map(likes.set(beverage, res.data.count)))
                        })
                })
                setPagesState({
                    ...pagesState,
                    currentPage: response.data.page + 1,
                    totalPages: response.data.pages
                });
            },
            (error) => {
                const msg = (error.response && error.response.data) || error.message || error.toString();
                setErrorMsg(msg);
            }
        );
    }

    useEffect(() => {
        getBeveragesInfoData();
    }, [pagesState.currentPage, filter]);

    const handlePageChange = (currentPage) => {
        setPagesState({...pagesState, currentPage: currentPage});
    }

    const handleBeverageShow = (beverageId) => {
        navigate("/library/beverage/" + beverageId);
    }

    const handleFilterChange = (e) => {
        setFilter({...filter, [e.target.name]: e.target.value});
        setPagesState({...pagesState, currentPage: 1});
    }

    const beverageInfo = (beverage) => {
        if (beverage !== undefined) {
            console.log(likes.get(beverage))
            return (
                <Grid item md={3} key={beverage.id}>
                    <MDBCard className="row-eq-height h-100">
                        <MDBCardHeader>
                            <div>
                                <i className="bi bi-heart-fill mr-2"
                                   style={{color: "red"}}/> {likes.get(beverage)} {likes.get(beverage) === 1 ? "like" :  "likes"}
                            </div>
                            <MDBCardImage className="w-100 h-10 card-img-top"
                                          src={PictureURL + "show?uuid=" + beverage.image}/>
                        </MDBCardHeader>
                        <MDBCardBody>
                            <MDBCardTitle className="display-7 text-center">{beverage.name}</MDBCardTitle>
                            <MDBCardText className="pe-4 ps-4">
                                <MDBRow className="justify-content-center text-muted">
                                    <ul>
                                        <li>Volume: {beverage.volume} л.</li>
                                        <li>Creator: {beverage.creator}</li>
                                        <li>Percentage: {beverage.percentage}%</li>
                                    </ul>
                                </MDBRow>
                                <MDBRow className="justify-content-center">
                                    <i className="fa fa-star"/>
                                    <i className="fa fa-star"/>
                                    <i className="fa fa-star"/>
                                    <i className="fa fa-star"/>
                                    <i className="fa fa-star"/>
                                </MDBRow>
                            </MDBCardText>
                        </MDBCardBody>
                        <MDBCardFooter>
                            <MDBBtnGroup className="d-flex align-items-center">
                                <MDBBtn color="dark" onClick={(e) => handleBeverageShow(beverage.id)}>
                                    View details
                                </MDBBtn>
                            </MDBBtnGroup>
                        </MDBCardFooter>
                    </MDBCard>
                </Grid>
            )
        } else {
            return null;
        }
    };

    const filterInput = () => {
        return (
            <MDBRow className="mt-4">
                <MDBRow className="justify-content-center">
                    <MDBCol md={4}>
                        <MDBInputGroup textBefore=">" className='mb-3'>
                            <input className='form-control' type='text' placeholder="Name"
                                   name="name" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                    <MDBCol md={2}>
                        <MDBInputGroup textBefore='>' className='mb-3'>
                            <input className='form-control' type='text' placeholder="Creator"
                                   name="creator" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                    <MDBCol md={2}>
                        <MDBInputGroup textBefore='>' className='mb-3'>
                            <input className='form-control' type='text' placeholder="Category"
                                   name="category" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                </MDBRow>
                <MDBRow className="justify-content-center">
                    <MDBCol md={2}>
                        <MDBInputGroup textBefore='>' className='mb-3'>
                            <input className='form-control' type='text' placeholder="Vol."
                                   name="volume" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                    <MDBCol md={2}>
                        <MDBInputGroup textBefore='>' className='mb-3'>
                            <input className='form-control' type='text' placeholder="%."
                                   name="percentage" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                    <MDBCol md={4}>
                        <MDBInputGroup textBefore='>' className='mb-3'>
                            <input className='form-control' type='text' placeholder="Composition"
                                   name="composition" onChange={handleFilterChange}/>
                        </MDBInputGroup>
                    </MDBCol>
                </MDBRow>
            </MDBRow>
        );
    }

    return (
        <MDBContainer>
            {filterInput()}
            <MDBRow className="mt-3 mb-3">
                <Grid container spacing={2}>
                    {beverages ? beverages.sort((a, b) => likes.get(b) - likes.get(a)).map((beverage) => beverageInfo(beverage)) : null}
                </Grid>
            </MDBRow>
            <MDBRow className="d-flex justify-content-center">
                <Pagination name="page"
                            value={pagesState.currentPage - 1}
                            totalPages={pagesState.totalPages}
                            showFirstLast={pagesState.totalPages >= 10}
                            atBeginEnd={0}
                            aroundCurrent={2}
                            onChange={(e) => {
                                const pageIndex = e.target.value + 1;
                                handlePageChange(pageIndex);
                            }}/>
            </MDBRow>
        </MDBContainer>
    );
};

