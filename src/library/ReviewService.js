import axios from "axios";
import AuthHeader from "../general/util/AuthHeader";
import { ReviewUrl } from "../url";

const getFeedbackForBeverage = (beverageId) => {
    return axios.get(ReviewUrl + "feedback", {
        headers: AuthHeader(),
        params: {
            beverageId: beverageId,
        }
    });
}

const postFeedback = (feedback) => {
    return axios.post(ReviewUrl + "feedback", {
        beverageId: feedback.beverageId,
        userName: feedback.userName,
        feedbackBody: feedback.feedbackBody,
        feedbackRating: feedback.feedbackRating,
        profileImgUUID: feedback.profileImgUUID,
    });
}

const removeFeedback = (feedbackId) => {
    return axios.post(ReviewUrl + "feedback/delete", {
        feedbackId: feedbackId
    });
}

const approveFeedback = (feedbackId) => {
    return axios.put(ReviewUrl + "feedback/approve", {
        feedbackId: feedbackId
    });
}

const getFeedbackForReview = () => {
    return axios.get(ReviewUrl + "feedback/onReview", {
        headers: AuthHeader()
    });
}

const ReviewService = {
    getFeedbackForBeverage,
    postFeedback,
    approveFeedback,
    removeFeedback,
    getFeedbackForReview
}

export default ReviewService