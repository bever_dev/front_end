import axios from "axios";
import {PreferenceURL} from "../url";
import AuthHeader from "../general/util/AuthHeader";

const addBeverageInUserFavorite = (beverageId, userId) => {
    return axios.post(PreferenceURL + "favorite", {
        beverageId: beverageId,
        userId: userId,
    },{
        headers: AuthHeader()
    });
};

const getAllFavoritesByUser = (userId) => {
    return axios.get(PreferenceURL + "favorites/user", {
        headers: AuthHeader(),
        params: {
            userId: userId
        }
    });
};

const getAllFavoritesByBeverage = (beverageId) => {
    return axios.get(PreferenceURL + "favorites/beverage", {
        headers: AuthHeader(),
        params: {
            beverageId: beverageId
        }
    });
};

const checkIfBeverageInUserFavorites = (beverageId, userId) => {
    return axios.get(PreferenceURL + "favorites/check", {
        headers: AuthHeader(),
        params: {
            beverageId: beverageId,
            userId: userId
        }
    });
};

const deleteFavoriteByItsIdAndUserId = (beverageId, userId) => {
    return axios.delete(PreferenceURL + "favorite", {
        headers: AuthHeader(),
        params: {
            beverageId: beverageId,
            userId: userId
        }
    });
};

const deleteFavoritesByUser = (userId) => {
    return axios.delete(PreferenceURL + "favorites/user", {
        headers: AuthHeader(),
        params: {
            userId: userId
        }
    });
};

const deleteFavoritesByBeverage = (beverageId) => {
    return axios.delete(PreferenceURL + "favorites/beverage", {
        headers: AuthHeader(),
        params: {
            beverageId: beverageId
        }
    });
};

const PreferService = {
    getAllFavoritesByBeverage,
    getAllFavoritesByUser,
    deleteFavoritesByUser,
    deleteFavoritesByBeverage,
    deleteFavoriteByItsIdAndUserId,
    checkIfBeverageInUserFavorites,
    addBeverageInUserFavorite
};

export default PreferService;
