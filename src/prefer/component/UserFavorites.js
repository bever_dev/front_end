import React, {useEffect, useState} from "react";
import PreferService from "../PreferService";
import AuthService from "../../auth/AuthService";
import LibraryService from "../../library/LibraryService";
import {Grid} from "@mui/material";
import {
    MDBBtn,
    MDBBtnGroup,
    MDBCard,
    MDBCardBody,
    MDBCardFooter,
    MDBCardHeader,
    MDBCardImage,
    MDBCardTitle, MDBContainer,
    MDBRow
} from "mdb-react-ui-kit";
import {PictureURL} from "../../url";
import {useNavigate} from "react-router-dom";

export default function UserFavorites() {
    const [beverages, setBeverages] = useState([]);
    const [errorMsg, setErrorMsg] = useState([]);
    let navigate = useNavigate();

    const fillBeveragesListByFavorites = (favoritesList) => {
        setBeverages([]);
        favoritesList.forEach(favorite => {
            console.log(favorite.beverageId)
            LibraryService.getBeverageDetails(favorite.beverageId).then(
                (response) => {
                    setBeverages(oldBeverages => [...oldBeverages, response.data.beverage[0]])
                },
                (error) => {
                    console.log(error);
                    const msg = (error.response && error.response.data) || error.message || error.toString();
                    setErrorMsg(msg);
                }
            );
        })
    }

    useEffect(() => {
        const user = AuthService.getCurrentUser();
        PreferService.getAllFavoritesByUser(user.userId).then(
            (response) => {
                fillBeveragesListByFavorites(response.data.favorites);
            },
            (error) => {
                const msg = (error.response && error.response.data) || error.message || error.toString();
                setErrorMsg(msg);
            }
        );
    }, []);

    const handleBeverageShow = (beverageId) => {
        navigate("/library/beverage/" + beverageId);
    }

    const favBeverage = (beverage) => {
        if (beverage !== undefined) {
            return (
                <Grid item md={4} key={beverage.id}>
                    <MDBCard className="row-eq-height h-100">
                        <MDBCardHeader>
                            <MDBCardImage className="w-100 h-10 card-img-top"
                                          src={PictureURL + "show?uuid=" + beverage.image}/>
                        </MDBCardHeader>
                        <MDBCardBody>
                            <MDBCardTitle className="display-7 text-center">{beverage.name}</MDBCardTitle>
                        </MDBCardBody>
                        <MDBCardFooter>
                            <MDBBtnGroup className="d-flex align-items-center">
                                <MDBBtn color="dark" onClick={(e) => handleBeverageShow(beverage.id)}>
                                    View details
                                </MDBBtn>
                            </MDBBtnGroup>
                        </MDBCardFooter>
                    </MDBCard>
                </Grid>
            )
        } else {
            return null;
        }
    };

    return (
        <MDBContainer>
            <MDBRow className="p-3 text-center">
                <h2>МОИ ИЗБРАННЫЕ НАПИТКИ ИЗ БИБЛИОТЕКИ</h2>
            </MDBRow>
            <MDBRow className="mt-3 mb-3">
                <Grid container spacing={2}>
                    { beverages ? beverages.map((beverage) => favBeverage(beverage)) : null }
                </Grid>
            </MDBRow>
        </MDBContainer>
    );

}
