import andrey from '../assets/team/andrey.png'
import ruslan from '../assets/team/ruslan.png'
import roman from '../assets/team/roman.png'
import mikhail from '../assets/team/mikhail.png'

const team = {
    info: [
        {
            name: '@m.brusnikin',
            image: mikhail,
            position: 'front-end',
        },
        {
            name: '@r.yatmanov',
            image: roman,
            position: 'back-end, front-end',
        },
        {
            name: '@r.popov',
            image: ruslan,
            position: 'front-end, back-end',
        },
        {
            name: '@a.rudometov',
            image: andrey,
            position: 'dev-ops, back-end',
        },
    ]
}
export default team