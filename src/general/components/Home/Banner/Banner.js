import React from 'react';
import './Banner.css'
import wave from '../../../../assets/bg-pattern.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'

export default function BannerTwo() {
            return (
                <section>
                    <div className="banner">
                        <div className="banner__layout">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                    <h2 classNmae="h1">Let's Get Started</h2>
                                    <div className="section-title-border"></div>
                                    <p>Покупая папе пиво, маленький Витя забыл слово «безалкогольное» и раскодировал отца.</p>
                                    <div className="hover-effect-px">
                                        <button>Get Started <FontAwesomeIcon icon={faArrowRight} /></button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className="image-wave">
                                <img src={wave} alt="wave" />
                            </div>
                        </div>
                    </div>
                </section>
            );
}
