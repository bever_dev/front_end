import React from 'react';
import './Header.css'

const Header = () => {
    return(
        <header>
            <div className="header-overlay">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <h1>One place to admire them all</h1>
                            <p>Community-driven beverage repository with biased reviews, unfunny preview images and unfair admins. More bells and whistles are coming, subscribe for updates!</p>
                            <form>
                                <input type="email" placeholder="Email" />
                                <button type="submit" className="btn-submit hover-effect-px">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;