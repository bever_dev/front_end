import React from 'react';
import './Work.css';
import { work } from '../../../../data/work'
import WorkItem from './WorkItem';

export default function Work() {
    const item = work.serve.map((item, index) => {
        return (
            <div className="col-lg-4 col-md-4 col-sm-12 work__items mt-3" key={index}>
                <WorkItem title={item.title} text={item.text} icon={item.icon} index={index}/>
            </div>
        )
    })
    return (
        <section className="work">
            <div className="container work__first-section">
                <div className="row">
                    <div className="col-lg-12 col-md-12 text-center">
                        <h2>WORK IN PROGRESS</h2>
                        <div className="section-title-border"></div>
                        <p className="par pt-4">
                            Мы обещаем множество сервисов, мы работаем над ними, мы сдвигаем сроки вправо, а задачи - в бэклог.
                        </p>
                    </div>
                </div>
                <div className="row mt-4 text-center">
                    {item}
                </div>
            </div>
        </section>
    );
}
