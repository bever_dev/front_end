import React from 'react';
import Header from './Header/Header';
import Team from './Team/Team';
import Work from './Work/Work'
import Banner from './Banner/Banner'


export default function Home() {
    return (
        <div>
            <Header />
            <Work/>
            <Team />
            <Banner/>
        </div>
    )
}