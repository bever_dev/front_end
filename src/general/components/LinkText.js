import React from 'react';
import cl from './LinkText.module.css'

const LinkText = ({children, ...props}) => {
    return (
        <text {...props} className={cl.linkText}>
            {children}
        </text>
    );
};

export default LinkText;