import React from 'react';
import { ReactComponent as Page404Img} from '../../images/404page.svg'
import cl from './Page404.module.css'

const Page404 = () => {
    return (
        <div className='d-flex justify-content-center align-self-center align-items-center '>
            <Page404Img className={cl.page404}/>
        </div>
    );
};

export default Page404;