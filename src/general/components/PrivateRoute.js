import React from 'react';
import { Navigate } from "react-router-dom";

const PrivateRoute = ({isAccessAllowed, redirectPath, element}) => {
    if (!isAccessAllowed) {
        return <Navigate to={redirectPath} replace/>
    }
    return element;
};

export default PrivateRoute;