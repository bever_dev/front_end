import { Outlet } from 'react-router-dom';
import NavBar from "./NavBar";

export default function Layout() {

    return(
        <div className="d-flex flex-column min-vh-100">
            <header>
                <NavBar/>
            </header>
            <main className="mb-5">
                <Outlet/>
            </main>
            <footer className="text-white-50 bg-dark mt-auto py-3 text-center">
                <div>&copy; BEVER 2022</div>
            </footer>
        </div>
    )
}
