import React, {useEffect, useState} from "react";
import {Link, useNavigate} from 'react-router-dom';
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import AuthService from "../../auth/AuthService";

export default function NavBar() {

    const [isModerator, setIsModerator] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);
    const [currentUser, setCurrentUser] = useState(undefined);
    const navigate = useNavigate();

    useEffect(() => {
        const user = AuthService.getCurrentUser();
        if (user && user.roles) {
            setCurrentUser(user);
            if (user.roles.includes('ROLE_MODERATOR')) {
                setIsModerator(true);
            }
            if (user.roles.includes('ROLE_ADMIN')) {
                setIsAdmin(true);
            }
        }
    }, []);

    const logout = () => {
        setCurrentUser(AuthService.logout());
        setIsAdmin(false);
        navigate("/");
    };

    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#home">BEVER</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link as={Link} to={"/"}>Home</Nav.Link>
                    {
                        currentUser ?
                            <>
                                <NavDropdown title="Library" id="basic-nav-dropdown">
                                    <NavDropdown.Item as={Link} to="/library/beverages">All Beverages</NavDropdown.Item>
                                </NavDropdown>
                            </> : null
                    }
                </Nav>
                <Nav>
                    {!currentUser ?
                        <>
                            <Nav.Link as={Link} to={"/signup"}>Sign Up</Nav.Link>
                            <Nav.Link as={Link} to={"/signin"}>Sign In</Nav.Link>
                        </>
                        :
                        <>
                            {
                                isModerator ? <Nav.Link as={Link} to={"/admin"}>Admin</Nav.Link> : null
                            }
                            <Nav.Link as={Link} to={"/profile"}>Profile</Nav.Link>
                            <Nav.Link onClick={logout}>Logout</Nav.Link>
                        </>
                    }
                </Nav>
            </Container>
        </Navbar>
    );
}
