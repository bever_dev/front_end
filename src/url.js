const { REACT_APP_AUTH_API_URL, REACT_APP_LIB_API_URL, REACT_APP_USER_API_URL,
    REACT_APP_REV_API_URL , REACT_APP_PIC_API_URL, REACT_APP_PREFER_API_URL} = process.env;

const AuthUrl = REACT_APP_AUTH_API_URL;

const LibraryUrl = REACT_APP_LIB_API_URL;

const UserUrl = REACT_APP_USER_API_URL;

const ReviewUrl = REACT_APP_REV_API_URL;

const PictureURL = REACT_APP_PIC_API_URL

const PreferenceURL = REACT_APP_PREFER_API_URL

export { AuthUrl, LibraryUrl, UserUrl, ReviewUrl, PictureURL, PreferenceURL}
