import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import UserService from "../UserService";
import AuthService from "../../auth/AuthService";
import PictureService from "../../library/PictureService";

const ChangeUserSettingsModal = ({isShow, closeModal, previousUserSettings, setUserSettings}) => {
    const [formState, setFormState] = useState(null);

    useEffect(() => {
        setFormState({
                nickname: previousUserSettings.nickname,
                firstName: previousUserSettings.firstName,
                surname: previousUserSettings.surname,
                middleName: previousUserSettings.middleName,
                email: previousUserSettings.email,
                profileImgUUID: previousUserSettings.profileImgUUID,
                oldPassword: '',
                newPassword: '',
                confNewPassword: ''
            }
        )
    }, [previousUserSettings]);


    const [errorMsg, setErrMsg] = useState(null);

    const handleImg = (e) => {
        if (e.target.files && e.target.files[0]) {
            PictureService.uploadBeveragePictureAndGetUUID(e.target.files[0]).then(
                response => {
                    console.log(response.data)
                    setFormState({...formState, profileImgUUID: response.data.uuid})
                })
        }
    }

    return (
        <Modal show={isShow} onHide={closeModal}>
            <Modal.Header closeButton>
                <Modal.Title>Измененить настройки пользователя</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <div className="mb-3">
                        <label htmlFor="formFile" className="form-label">Выберите аватар</label>
                        <input onChange={handleImg} className="form-control" type="file" id="formFile"/>
                    </div>
                    <Form.Group className="mb-2" controlId="formNickname">
                        <Form.Label>Firstname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={previousUserSettings.firstName}
                            placeholder="Firstname"
                            onChange={(e) => setFormState({...formState, firstName: e.target.value})}
                        />

                        <Form.Label>Surname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={previousUserSettings.surname}
                            placeholder="Surname"
                            onChange={(e) => setFormState({...formState, surname: e.target.value})}

                        />

                        <Form.Label>Middlename</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={previousUserSettings.middleName}
                            placeholder="Middlename"
                            onChange={(e) => setFormState({...formState, middleName: e.target.value})}
                        />

                        <Form.Label>Nickname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={previousUserSettings.nickname}
                            placeholder="Nickname"
                            onChange={(e) => setFormState({...formState, nickname: e.target.value})}
                        />

                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={previousUserSettings.email}
                            placeholder="Email"
                            onChange={(e) => setFormState({...formState, email: e.target.value})}
                        />

                        <Form.Label>Change password</Form.Label>
                        <Form.Control
                            className='mb-1'
                            type="test"
                            defaultValue={''}
                            placeholder="Old password"
                            onChange={(e) => setFormState({...formState, oldPassword: e.target.value})}
                        />
                        <Form.Control
                            className='mb-1'
                            type="test"
                            defaultValue={''}
                            placeholder="New password"
                            onChange={(e) => setFormState({...formState, newPassword: e.target.value})}
                        />
                        <Form.Control
                            type="text"
                            defaultValue={''}
                            placeholder="Confirm password"
                            onChange={(e) => setFormState({...formState, confNewPassword: e.target.value})}
                        />
                    </Form.Group>
                    <Form.Group>
                        {
                            errorMsg ? <div className="alert alert-danger" role="alert">{errorMsg}</div> : null
                        }
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={() => {
                    if (formState.newPassword !== formState.confNewPassword) {
                        setErrMsg('Different new passwords!');
                    } else {

                        UserService.changeUserSettings(formState).then(
                            (response) => {
                                const curUser = AuthService.getCurrentUser();
                                console.log(response);
                                curUser.jwtToken = response.data.jwtToken;
                                localStorage.setItem("user", JSON.stringify(curUser));
                                setUserSettings(response.data.modifiedUser);
                                setErrMsg(null);
                                closeModal();
                            },
                            (error) => {
                                setErrMsg(error.message)
                            }
                        )
                    }
                }}>Изменить</Button>
                <Button variant="danger" onClick={closeModal}>Отмена</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ChangeUserSettingsModal;