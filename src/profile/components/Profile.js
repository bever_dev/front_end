import {Button, Card, CardImg, Container, Form, FormGroup} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import UserService from "../UserService";
import ChangeUserSettingsModal from "./ChangeUserSettingsModal";
import {PictureURL} from "../../url";
import {useNavigate} from "react-router-dom";

export default function Profile() {
    const [isLoading, setIsLoading] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');
    const [isShowChangeUserSettingsModal, setIsShowChangeUserSettingsModal] = useState(false);
    const hideChangeUserSettingsModal = () => setIsShowChangeUserSettingsModal(false);
    const showChangeUserSettingsModal = () => setIsShowChangeUserSettingsModal(true);
    const [userSettings, setUserSettings] = useState({
        nickname: '',
        firstName: '',
        surname: '',
        middleName: '',
        email: '',
        profileImgUUID: ''
    });
    let navigate = useNavigate();

    function getUserFromStorage() {
        setIsLoading(true);
        UserService.getDefaultUserInfo().then(
            (response) => {
                console.log(response.data)
                setUserSettings(response.data);
                setIsLoading(false);
            },
            (error) => {
                const msg = (error.response && error.response.data) || error.message || error.toString();
                setErrorMsg(msg);
                setIsLoading(false);
            }
        );
    }

    useEffect(() => {
        getUserFromStorage();
    }, []);


    return (
        <div>
            {!isLoading ?
                <div>
                    <Container className="md mt-3 d-flex justify-content-center">
                        <Card className="card-container text-center">
                            <CardImg
                                src={PictureURL + "show?uuid=" + userSettings.profileImgUUID}
                                alt="profile-img"
                                className="profile-img-card"
                            />
                            <Form>
                                <FormGroup>
                                    <div className='mt-3 text-center display-6'>{userSettings.nickname}</div>
                                    <Form.Label
                                        className='mb-1 '>{userSettings.firstName} {userSettings.surname} {userSettings.middleName}</Form.Label>
                                    <Form.Label className='mb-3'>{userSettings.email}</Form.Label>
                                    <Button type="submit" className="me-4 btn btn-light btn-block" onClick={(e) => {
                                        e.preventDefault();
                                        showChangeUserSettingsModal();
                                    }}>Изменить профиль</Button>
                                    <Button type="submit" className="me-4 btn btn-success btn-block" onClick={(e) => {
                                        e.preventDefault();
                                        navigate("/profile/favorites");
                                    }}>Избранные напитки</Button>
                                </FormGroup>
                                <Form.Group>
                                    {
                                        errorMsg ? <div className="alert alert-danger" role="alert">{errorMsg}</div> : null
                                    }
                                </Form.Group>
                            </Form>
                        </Card>
                    </Container>
                    <ChangeUserSettingsModal isShow={isShowChangeUserSettingsModal} previousUserSettings={userSettings} closeModal={hideChangeUserSettingsModal} setUserSettings={setUserSettings}/>
                </div>
                : <div className="d-flex justify-content-center mt-5">
                    <div className="spinner-border" role="status">
                    </div>
                </div>
            }
        </div>
    );

}
