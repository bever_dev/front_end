import React, {useRef} from 'react';
import {Button, Form, FormControl, InputGroup, Modal} from "react-bootstrap";
import PictureService from "../../library/PictureService";


const AddDrinkModalWindow = ({show, handleClose, addDrink}) => {
    let formState = {
        creator: "",
        name: "",
        category: "",
        volume: 0.5,
        percentage: 0,
        price: 0,
        composition: "",
        available: 0,
        image: ""
    };

    const imgFile = useRef()

    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Добавление напитка</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-2" controlId="formAddDrink">
                        <Form.Control className='mb-2' onChange={e => formState.creator = e.target.value} type="text"
                                      placeholder="Создатель напитка"/>
                        <Form.Control className='mb-2' onChange={e => formState.name = e.target.value} type="text"
                                      placeholder="Название напитка"/>
                        <Form.Control className='mb-2' onChange={e => formState.category = e.target.value} type="text"
                                      placeholder="Категория"/>
                        <Form.Control className='mb-2' onChange={e => formState.volume = e.target.value} type="number" step="0.01"
                                      min={0} placeholder="Объем"/>
                        <Form.Control className='mb-2' onChange={e => formState.percentage = e.target.value} type="number" step="0.01"
                                      min={0} max={95} placeholder="Крепкость"/>
                        <Form.Control className='mb-2' onChange={e => formState.price = e.target.value} type="number" step="0.01" min={0}
                                      placeholder="Цена"/>
                        <Form.Control className='mb-2' onChange={e => formState.composition = e.target.value} type="text"
                                      placeholder="Состав"/>
                        <Form.Control className='mb-2' onChange={e => formState.available = e.target.value} type="number" step="1"
                                      min={0} placeholder="Количество"/>
                    </Form.Group>
                </Form>
                <div className="mb-3">
                    <label htmlFor="formFile" className="form-label">Выберите изображение напитка</label>
                    <input ref={imgFile} className="form-control" type="file" id="formFile"/>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={() => {
                    PictureService.uploadBeveragePictureAndGetUUID(imgFile.current.valueOf().files[0]).then(
                        res => {
                            console.log(res.data.uuid)
                            addDrink({
                                "creator": formState.creator,
                                "name": formState.name,
                                "category": formState.category,
                                "volume": formState.volume,
                                "percentage": formState.percentage,
                                "price": formState.price,
                                "composition": formState.composition,
                                "rating": 0,
                                "available": formState.available,
                                "image": res.data.uuid
                            })
                        })
                    handleClose()
                }}>Добавить</Button>
                <Button variant="danger" onClick={handleClose}>Сбросить</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default AddDrinkModalWindow;