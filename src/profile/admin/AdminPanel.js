import React, {useState} from 'react';
import {Alert, Button, Card, CardImg, Container, Form} from "react-bootstrap";
import AddDrinkModalWindow from "./AddDrinkModalWindow";
import LibraryService from "../../library/LibraryService";
import {useNavigate} from "react-router-dom";
import UserService from "../UserService";

const AdminPanel = () => {
    const navigate = useNavigate();
    const [isShowAddDrinkModal, setIsShowAddDrinkModal] = useState(false);
    const showAddDrinkModal = () => setIsShowAddDrinkModal(true);
    const hideAddDrinkModal = () => setIsShowAddDrinkModal(false);

    return (
        <Container className="mt-3 d-flex justify-content-center">
            <Card className="card-container text-center">
                <Form>
                    <Form.Label className='display-6 mb-3'>Админ панель</Form.Label>
                    <Button type="submit" className="me-4 btn btn-light btn-block" onClick={() => {navigate('/admin/reviews')}}>Модерация отзывов</Button>
                    {
                        UserService.isAdmin() ?
                            <>
                                <Button type="submit" className="me-4 btn btn-light btn-block" onClick={() => {navigate('/admin/users')}}>Список пользователей</Button>
                                <Button type="submit" className="me-4 btn btn-light btn-block" onClick={(e) => {
                                    e.preventDefault();
                                    showAddDrinkModal();
                                }}>Добавить напиток</Button>
                            </>
                            : null
                    }
                </Form>
            </Card>
            <AddDrinkModalWindow show={isShowAddDrinkModal} handleClose={hideAddDrinkModal} addDrink={LibraryService.addBeverage}/>
        </Container>
    );
};

export default AdminPanel;