import React, {useEffect, useState} from 'react';
import UserService from "../UserService";
import Table from 'react-bootstrap/Table'
import LinkText from "../../general/components/LinkText";
import {Button, Form} from "react-bootstrap";
import {FormGroup} from "@mui/material";
import ViewUserSettingsModal from "./ViewUserSettingsModal";

const UsersList = () => {
    const [isUsersListLoading, setIsUserListsLoading] = useState(false);
    const [users, setUsers] = useState([]);
    const [errorMsg, setErrorMsg] = useState('');
    const [selectedUser, setSelectedUser] = useState({});
    const [showViewUserSettingsModal, setShowViewUserSettingsModal] = useState(false);
    const [searchFilter, setSearchFilter] = useState({
        nickname: '',
        email: ''
    })

    const showViewUserModal = () => {
        setShowViewUserSettingsModal(true)
    }

    const hideViewUserModal = () => {
        setShowViewUserSettingsModal(false)
    }

    const deleteUser = (userId) => {
        UserService.deleteUserById(userId).then((response) => {
            setUsers(users.filter((user) => {
                return user.id !== userId;
            }));
        }, (error) => {
            console.log(error);
        });
    }

    useEffect(() => {
        setIsUserListsLoading(true);
        UserService.getUsers().then(
            (response) => {
                setUsers(response.data);
                setIsUserListsLoading(false);
                console.log(users);
            },
            (error) => {
                setErrorMsg(error.message());
                setIsUserListsLoading(false);
            })
    }, [])

    return (
        <div className='d-flex flex-column justify-content-center align-items-center mt-3'>
            <text className='display-6 mb-3'>Список пользователей</text>
            <Form className='w-25 mb-3'>
                <FormGroup>
                    <Form.Control
                        className='mb-1'
                        type="text"
                        name="nicknameForm"
                        placeholder="Nickname"
                        onChange={(e) => {
                            setSearchFilter({...searchFilter, nickname: e.target.value})
                            console.log(searchFilter);
                        }}
                    />
                    <Form.Control
                        className='mb-1'
                        type="text"
                        name="emailForm"
                        placeholder="Email"
                        onChange={(e) => {
                            setSearchFilter({...searchFilter, email: e.target.value})
                            console.log(searchFilter);
                        }}
                    />
                </FormGroup>
            </Form>
            {
                !isUsersListLoading ? <Table bordered striped hover size='sm' className='text-center w-50'>
                        <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Nickname</th>
                            <th>Fullname</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            users.filter((user) => {
                                return user.nickname.includes(searchFilter.nickname) && user.email.includes(searchFilter.email)
                            }).map((user) => {
                                return <tr>
                                    <td>{user.id}</td>
                                    <td>{user.nickname}</td>
                                    <td>{user.firstName} {user.surname} {user.middleName}</td>
                                    <td>{user.email}</td>
                                    <td className='d-flex justify-content-sm-around'>
                                        <LinkText onClick={() => {
                                            setSelectedUser(user);
                                            console.log('After click, ID: ' + users.indexOf(user));
                                            showViewUserModal();
                                        }
                                        }>Открыть</LinkText>
                                        <LinkText onClick={() => deleteUser(user.id)}>Удалить</LinkText>
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                    </Table>
                    : <div className="d-flex justify-content-center mt-5">
                        <div className="spinner-border" role="status">
                        </div>
                    </div>
            }
            <ViewUserSettingsModal userSettings={selectedUser} closeModal={hideViewUserModal}
                                   isShow={showViewUserSettingsModal} setUsersList={setUsers} usersList={users}/>

        </div>

    );
};

export default UsersList;