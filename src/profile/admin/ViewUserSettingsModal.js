import React, {useEffect, useState} from 'react';
import {Button, Form, Modal} from "react-bootstrap";
import UserService from "../UserService";

const ViewUserSettingsModal = ({isShow, closeModal, userSettings, usersList, setUsersList}) => {
    const [formState, setFormState] = useState(null);

    useEffect(() => {
        setFormState({
                id: userSettings.id,
                nickname: userSettings.nickname,
                firstName: userSettings.firstName,
                surname: userSettings.surname,
                middleName: userSettings.middleName,
                email: userSettings.email,
                oldPassword: '',
                newPassword: '',
            }
        )
    }, [userSettings]);

    const [errorMsg, setErrMsg] = useState(null);

    return (
        <Modal show={isShow} onHide={closeModal}>
            <Modal.Header closeButton>
                <Modal.Title>Измененить настройки пользователя</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-2" controlId="formNickname">
                        <Form.Label>Firstname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={userSettings.firstName}
                            placeholder="Firstname"
                            onChange={(e) => setFormState({...formState, firstName: e.target.value})}
                        />

                        <Form.Label>Surname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={userSettings.surname}
                            placeholder="Surname"
                            onChange={(e) => setFormState({...formState, surname: e.target.value})}

                        />

                        <Form.Label>Middlename</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={userSettings.middleName}
                            placeholder="Middlename"
                            onChange={(e) => setFormState({...formState, middleName: e.target.value})}
                        />

                        <Form.Label>Nickname</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={userSettings.nickname}
                            placeholder="Nickname"
                            onChange={(e) => setFormState({...formState, nickname: e.target.value})}
                        />

                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="text"
                            defaultValue={userSettings.email}
                            placeholder="Email"
                            onChange={(e) => setFormState({...formState, email: e.target.value})}
                        />
                    </Form.Group>
                    <Form.Group>
                        {
                            errorMsg ? <div className="alert alert-danger" role="alert">{errorMsg}</div> : null
                        }
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={() => {
                        console.log(formState);
                        UserService.changeUserSettingsById(formState, userSettings.id).then(
                            (response) => {
                                setErrMsg(null);
                                const index = usersList.indexOf(userSettings);
                                let newUsersList = [...usersList];
                                console.log(index);
                                newUsersList[index] = {
                                    ...userSettings,
                                    nickname: formState.nickname,
                                    firstName: formState.firstName,
                                    surname: formState.surname,
                                    middleName: formState.middleName,
                                    email: formState.email
                                };
                                setUsersList(newUsersList)
                                closeModal();
                            },
                            (error) => {
                                setErrMsg(error.message);
                            })
                }}>Изменить</Button>
                <Button variant="danger" onClick={closeModal}>Отмена</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ViewUserSettingsModal;