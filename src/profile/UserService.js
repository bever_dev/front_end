import axios from "axios";
import AuthHeader from "../general/util/AuthHeader";
import {UserUrl} from "../url";
import AuthService from "../auth/AuthService";

const isAuth = () => {
    return AuthService.getCurrentUser() !== null;
}

const isModerator = () => {
    const user = AuthService.getCurrentUser();
    return user !== null && user.roles.includes('ROLE_MODERATOR');
}

const isAdmin = () => {
    const user = AuthService.getCurrentUser();
    return user !== null && user.roles.includes('ROLE_ADMIN');
}

const getUsers = () => {
    return axios.get( UserUrl + 'users/', {headers: AuthHeader()});
};

const getDefaultUserInfo = () => {
    return axios.get( UserUrl + 'user/', {headers: AuthHeader()});
};

const changeUserSettings = (userSettings) => {
    return axios.put(UserUrl + 'user/', {
        nickname: userSettings.nickname,
        firstName: userSettings.firstName,
        middleName: userSettings.middleName,
        surname: userSettings.surname,
        oldPassword: userSettings.oldPassword,
        newPassword: userSettings.newPassword,
        email: userSettings.email,
        profileImgUUID: userSettings.profileImgUUID,
    }, {
        headers: AuthHeader()
    });
}

const changeUserSettingsById = (userSettings, userId) => {
    return axios.put(UserUrl + 'user/' + userId, {
        nickname: userSettings.nickname,
        firstName: userSettings.firstName,
        middleName: userSettings.middleName,
        surname: userSettings.surname,
        oldPassword: userSettings.oldPassword,
        newPassword: userSettings.newPassword,
        email: userSettings.email
    }, {
        headers: AuthHeader()
    });
}

const deleteUserById = (userId) => {
    return axios.delete(UserUrl + 'user/' + userId, {
        headers: AuthHeader()
    });
}

const UserService = {
    getUsers,
    getDefaultUserInfo,
    changeUserSettings,
    changeUserSettingsById,
    deleteUserById,
    isAuth,
    isModerator,
    isAdmin
};

export default UserService;
